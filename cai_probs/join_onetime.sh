#!/bin/bash
# path
path=$1
# nodes
nodes=$2
# output time, for example, 0430
otime=$3
# output file
output='flarecs.join.'$otime'.vtk'

# join script
join_vtk='/data1/home/shenccai/workdir/athena_develop/cai_probs/join_vtk'

i=0
files=$path'id'$i'/''flarecs.'$otime'.vtk'

for ((i=1;i<=63;i++)) do
	newfile=$path'id'$i'/''flarecs-id'$i'.'$otime'.vtk'
	files=${files}" "$newfile
done

#join_vtk
$join_vtk -o $output $files
